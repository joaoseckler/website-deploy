#!/bin/sh

cd files/{{ name }}

echo "Running npm install..."
npm install

echo "Running npm run build install..."
npm run build

echo "Compressing build..."
cd build
tar -czf ../build.tar.gz *
