SHELL=bash
VERBOSE ?= -v



.PHONY: all help

help:
	@echo "Use make <playbook> to run playbook in ./playbook folder."
	@echo "    Use TAGS=tag1:tag2:tag3 to pass tags to call"
	@echo "    Use make all to make all playbooks"

all: packages db server partituras elami laialaia umami website repetition

list:
	@echo Available playbooks:
	@echo
	@ls -1 playbooks | sed 's/.yml$$//' | grep -Pv '_local$$' | grep -Pv 'common_vars' | sort

%:
	@gpg --decrypt --quiet secrets.asc | grep -v '^#' > /tmp/tmp$$$$.yml; \
	if [ -n "$(TAGS)" ]; then tags="--tags $(TAGS)"; fi; \
	if [ -f "playbooks/$@_local.yml" ]; then \
		jetp local $(VERBOSE) -p playbooks/$@_local.yml -r roles/; \
	fi; \
	jetp ssh $(VERBOSE) -p playbooks/$@.yml -i inventory/ -r roles/ \
	--extra-vars @/tmp/tmp$$$$.yml $$tags ; \
	rm /tmp/tmp$$$$.yml
