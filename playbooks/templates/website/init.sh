#!/bin/bash
set -a
. .env
set +a

rmdir --ignore-fail-on-non-empty venv

if [ ! -d venv ]
then
  python3 -m venv venv
fi

mkdir -p "{{ static_folder }}" "{{ media_folder }}"
chown -R www-data:www-data "{{ static_folder }}"
chown -R www-data:www-data "{{ media_folder }}"

. venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
pip install psycopg2-binary gunicorn

pip install -r requirements.txt
{{#if run_collectstatic }}python manage.py collectstatic --noinput{{/if }}
{{#if run_makemigrations }}python manage.py makemigrations --noinput{{/if }}
{{#if run_migrate }}python manage.py migrate --noinput{{/if }}
{{#if run_makemessages }}python manage.py makemessages --all{{/if }}
{{#if run_compilemessages }}python manage.py compilemessages{{/if }}
{{#if run_createsuperuser }}python manage.py createsuperuser --noinput{{/if }}
{{#if django_extra_commands is defined }}{{ django_extra_commands }}{{/if }}

