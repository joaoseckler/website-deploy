#!/bin/sh

sudo -u postgres sh << EOF
psql -c "create database {{ db_name }}"
psql -c "create user {{ db_user }} with encrypted password '{{ db_password }}'"
psql -c "alter database {{ db_name }} owner to {{ db_user }}"
EOF
