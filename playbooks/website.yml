- name: Deploy main website
  vars:
    project_name: jseckler.xyz
    django_project_name: joaoseckler
    repo: https://gitlab.com/joaoseckler/website
    branch: master
    debug: 0
    allowed_hosts: "jseckler.xyz www.jseckler.xyz 146.190.154.117"
    laialaia_front_url: https://jseckler.xyz/cifras

    # django static and media configuration
    static_url: "[]"
    static_folder: "static"
    media_url: '--'
    media_folder: "media"

    # django management commands
    run_collectstatic: true
    run_migrate: true
    run_makemigrations: true
    run_createsuperuser: true
    run_makemessages: false
    run_compilemessages: true
    django_extra_commands: |
      python ./makepages.py
      python ./manage.py register_cifra -au
      python ./manage.py register_page -au

    gunicorn_options: "--workers 3 --timeout 3000"

  vars_files:
    - common_vars.yml

  groups:
    - hosts

  tasks:
    - !git
      name: checkout repository
      repo: "{{ repo }}"
      branch: "{{ branch }}"
      path: "{{ website_folder }}"
      and:
        notify: updated

    - !shell
      name: pull submodules
      cmd: |
        cd {{ website_folder }}
        git submodule update --init --recursive --remote
        git pull --recurse-submodules

    - !directory
      name: create media and static directories
      path: "{{ base_serve_folder }}/{{ project_name }}/{{ item }}"
      attributes:
        owner: www-data
        group: www-data
        mode: 0o755
      with:
        items:
          - "{{ media_folder }}"
          - "{{ static_folder }}"

    - !file
      name: create log file
      path: /var/log/jseckler.xyz.log
      attributes:
        owner: www-data
        group: www-data
        mode: 0o640

    - !template
      name: copy env, init and update scripts
      src: "website/{{ item }}"
      dest: "{{ website_folder }}/{{ item }}"
      attributes:
        owner: root
        group: root
        mode: 0o744
      with:
        items:
          - init.sh
          - update.sh
          - .env
      and:
        notify: initialize

    - !template
      name: copy systemd service file for gunicorn
      src: website/gunicorn.service
      dest: /etc/systemd/system/gunicorn.service
      and:
        notify: reload_systemd

    - !template
      name: copy nginx files
      src: website/jseckler.xyz.location
      dest: /etc/nginx/conf.d/jseckler.xyz.location
      and:
        notify: restart_nginx


    - !directory
      name: check if venv folder exists
      path: "{{ website_folder }}/venv"
      and:
        notify: initialize

    - !shell
      name: link to partituras folder
      cmd: |
        cd {{ website_folder}}
        mkdir -p partituras/static/
        rm -f partituras/static/partituras
        ln -s /root/partituras partituras/static/partituras

  handlers:
    - !shell
      name: reload systemd
      cmd: |
        systemctl daemon-reload
        systemctl enable gunicorn
        systemctl restart gunicorn
        systemctl restart nginx
      with:
        subscribe: reload_systemd

    - !shell
      name: run initialization routine
      cmd: |
        cd {{ website_folder }}
        ./init.sh
      with:
        subscribe: initialize

    - !shell
      name: run update routine
      cmd: |
        cd {{ website_folder }}
        ./update.sh
      with:
        subscribe: updated

    - !sd_service
      name: restart nginx
      service: nginx
      started: true
      enabled: true
      with:
        subscribe: restart_nginx

    - !sd_service
      name: restart gunicorn
      service: gunicorn
      started: true
      enabled: true
      with:
        subscribe: updated

    - !sd_service
      name: restart gunicorn
      service: gunicorn
      started: true
      enabled: true
      with:
        subscribe: initialized
